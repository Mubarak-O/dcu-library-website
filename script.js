$(document).ready(function(){
    // Dropdown Functionality for CATALOG Tab
    $(".default-option-section").click(function(){
        $(".dropdown-section ul").toggleClass("active");
    });

    $(".dropdown-section ul li").click(function(){
        var text = ($(this).text());
        $(".default-option-section").text(text);
        $(".dropdown-section ul").removeClass("active");
    });
    
    // Dropdown Functionality for FACULTY Tab
    $(".default-option-faculty").click(function(){
        $(".dropdown-faculty ul").toggleClass("active");
    });

    $(".dropdown-faculty ul li").click(function(){
        var text = ($(this).text());
        $(".default-option-faculty").text(text);
        $(".dropdown-faculty ul").removeClass("active");
    });

    // Function which converts a string to Title Case
    function toTitleCase(string) {
        return string.replace(
        /\w\S*/g,
        function(txt) {
            return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
        });
    }

    // Method to locate a book given the user's input
    $(".search-field i").click(function(){
        var userInput = document.getElementById('search').value;
        var section = toTitleCase(document.getElementsByClassName("default-option-section")[0].innerText);
        var faculty = toTitleCase(document.getElementsByClassName("default-option-faculty")[0].innerText);
        
        if (userInput && (section != "Section") && (faculty != "Faculty")) {
            console.log(`Book: ${userInput}\nIn: ${section}\nFaculty: ${faculty}`);
            
            $.get('books.json')
                .done(data => {
                    if ((data.some(item => item.bookName.includes(userInput)) || data.some(item => item.bookName.toLowerCase().includes(userInput.toLowerCase()))) == true) {
                        console.log("Book: " + userInput + " found");
                        if (userInput.includes("Computer")) {
                            window.location.replace("bookpage.html");
                        }
                        else if (userInput.includes("Time")) {
                            window.location.replace("bookpage2.html");
                        }
                        else if (userInput.includes("Invest")){
                            window.location.replace("bookpage3.html");
                        }
                    }
                    else if ((data.some(item => item.author.includes(userInput)) || data.some(item => item.author.toLowerCase().includes(userInput.toLowerCase()))) == true) {
                        console.log("Author: " + userInput + " found");
                        if (userInput.includes("Charles Petzold")) {
                            window.location.replace("bookpage.html");
                        }
                        else if (userInput.includes("Stephen Hawking")) {
                            window.location.replace("bookpage2.html");
                        }
                        else if (userInput.includes("Benjamin Graham")){
                            window.location.replace("bookpage3.html");
                        }
                    }
                    else {
                        console.log("Book: " + userInput + " could not be found!");
                        window.location.replace("catalog.html");
                    }
                });
        }
        else {
            alert("Please make sure you have entered in the name of a Book or Author and that you have selected a Section and Faculty dropdown option!");
        }
    });
});